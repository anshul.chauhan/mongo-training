package com.training;

import com.mongodb.MongoClient;
import com.training.dao.MovieDao;
import com.training.model.Movie;
import org.bson.Document;

import javax.print.Doc;

public class StarterApplication {

    public static void main(String[] args) {
        MongoClient mongoClient = new MongoClient("localhost");
        MovieDao movieDao = new MovieDao(mongoClient);
        System.out.println(movieDao.getMovieById("573a1390f29313caabcd6223"));
        Movie movie = getMovie();
        System.out.println(movieDao.updateMovie("60eb00c2d148f463ee106e7b", getMovieUpdates()));
//        movieDao.insertMovie(movie);
//        movieDao.deleteMovie("60ec1ced398ba41eb49e9591");
    }

//    static Document getMovie() {
//        Document movie = new Document();
//        movie.put("title", "Test Movie");
//        movie.put("year", 2021);
//        return movie;
//    }

    static Movie getMovie() {
        Movie movie = new Movie();
        movie.setTitle("Test Movie");
        movie.setYear(2021);
        return movie;
    }

    static Document getMovieUpdates() {
        Document document = new Document();
        document.put("runtime", 30);
        document.put("year", 2021);
        return document;
    }
}
