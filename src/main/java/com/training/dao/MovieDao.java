package com.training.dao;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.ReturnDocument;
import com.training.codec.MovieCodec;
import com.training.codec.MovieConverter;
import com.training.model.Movie;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class MovieDao {

    private MongoCollection<Movie> moviesCollection;
    private CodecRegistry pojoCodecRegistry;

    private static final String DB_NAME = "sample_mflix";
    private static final String COLLECTION_NAME = "movies";

    public MovieDao(MongoClient mongoClient) {
        Codec<Document> documentCodec = MongoClientSettings.getDefaultCodecRegistry().get(Document.class);
        MovieConverter movieConverter = new MovieConverter();
        MovieCodec movieCodec = new MovieCodec(documentCodec, movieConverter);
        pojoCodecRegistry = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromCodecs(movieCodec));

        moviesCollection = mongoClient.getDatabase(DB_NAME).getCollection(COLLECTION_NAME, Movie.class).withCodecRegistry(pojoCodecRegistry);
    }

//    public Document getMovieById(String id) {
//        Document query = new Document("_id", new ObjectId(id));
//        MongoIterable<Document> results = moviesCollection.find(query);
//        if (results != null) {
//            for (Document result : results) {
//                return result;
//            }
//        }
//        return null; // or throw exception
//    }

    public Movie getMovieById(String id) {
        Bson queryFilter = Filters.eq("_id", new ObjectId(id));
        List<Movie> movies = new ArrayList<>();
        MongoIterable<Movie> results = moviesCollection.find(queryFilter);
        if (results != null) {
            results.iterator().forEachRemaining(movies::add);
        }
        return movies.isEmpty() ? null : movies.get(0); // or throw exception
    }

//    public void insertMovie(Document movie) {
//        moviesCollection.insertOne(movie);
//    }

    public void insertMovie(Movie movie) {
        moviesCollection.insertOne(movie);
    }

    public Movie updateMovie(String id, Document updateDoc) {
        Bson query = Filters.eq("_id", new ObjectId(id));
        Bson update = new Document("$set", updateDoc);
        FindOneAndUpdateOptions findOneAndUpdateOptions = new FindOneAndUpdateOptions();
        findOneAndUpdateOptions.returnDocument(ReturnDocument.AFTER);
        return moviesCollection.findOneAndUpdate(query, update, findOneAndUpdateOptions);
    }

    public void deleteMovie(String id) {
        Document query = new Document("_id", new ObjectId(id));
        moviesCollection.deleteOne(query);
    }
}
