package com.training.codec;

import com.training.model.Movie;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

public class MovieCodec implements Codec<Movie> {

    private Codec<Document> documentCodec;
    private MovieConverter movieConverter;

    public MovieCodec(Codec<Document> documentCodec, MovieConverter movieConverter) {
        this.documentCodec = documentCodec;
        this.movieConverter = movieConverter;
    }

    @Override
    public Movie decode(BsonReader reader, DecoderContext decoderContext) {
        Document movieDoc = documentCodec.decode(reader, decoderContext);
        return movieConverter.convert(movieDoc);
    }

    @Override
    public void encode(BsonWriter writer, Movie value, EncoderContext encoderContext) {
        Document movieDoc = movieConverter.convert(value);
        documentCodec.encode(writer, movieDoc, encoderContext);
    }

    @Override
    public Class<Movie> getEncoderClass() {
        return Movie.class;
    }
}
