package com.training.codec;

import com.training.model.Movie;
import org.bson.Document;
import org.bson.types.ObjectId;

public class MovieConverter {

    public Document convert(Movie movie) {
        Document movieDoc = new Document();
        if (movie.getId() != null)
            movieDoc.put("_id", new ObjectId(movie.getId()));
        if (movie.getPlot() != null)
            movieDoc.put("plot", movie.getPlot());
        if (movie.getGenres() != null)
            movieDoc.put("genres", movie.getGenres());
        if (movie.getRuntime() != null)
            movieDoc.put("runtime", movie.getRuntime());
        if (movie.getCast() != null)
            movieDoc.put("cast", movie.getCast());
        if (movie.getTitle() != null)
            movieDoc.put("title", movie.getTitle());
        if (movie.getFullPlot() != null)
            movieDoc.put("fullplot", movie.getFullPlot());
        if (movie.getLanguages() != null)
            movieDoc.put("languages", movie.getLanguages());
        if (movie.getReleased() != null)
            movieDoc.put("released", movie.getReleased());
        if (movie.getDirectors() != null)
            movieDoc.put("directors", movie.getDirectors());
        if (movie.getYear() != null)
            movieDoc.put("year", movie.getYear());
        return movieDoc;
    }

    public Movie convert(Document movieDoc) {
        Movie movie = new Movie();
        movie.setId(movieDoc.getObjectId("_id").toString());
        movie.setPlot(movieDoc.getString("plot"));
        movie.setGenres(movieDoc.getList("genres", String.class));
        movie.setRuntime(movieDoc.getInteger("runtime"));
        movie.setCast(movieDoc.getList("cast", String.class));
        movie.setTitle(movieDoc.getString("title"));
        movie.setFullPlot(movieDoc.getString("fullplot"));
        movie.setLanguages(movieDoc.getList("languages", String.class));
        movie.setReleased(movieDoc.getDate("released"));
        movie.setDirectors(movieDoc.getList("directors", String.class));
        movie.setYear(movieDoc.getInteger("year"));
        return movie;
    }
}
